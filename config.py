class Config:
    DB_USER = "postgres"
    DB_PASS = "postgres"
    DB_ADDR = "localhost:5432"
    DB_NAME = ""
    SQLALCHEMY_DATABASE_URI = ""
    SQLALCHEMY_ECHO = False

class DevConfigi(Config):
    pass

class ProdConfig(Config):
    pass
